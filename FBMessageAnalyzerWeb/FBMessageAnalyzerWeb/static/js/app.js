﻿// Hook up the upload button to upload file using ajax

var message_list = [];
$(document).ready(function () {
    $('#upload-file-btn').click(function () {
        for (var i = 0; i < message_list.length; i++) {
            message_list[i].remove();
        }
        $("#spinner").show();
        var form_data = new FormData($('#upload-file')[0]);
        $.ajax({
            type: 'POST',
            url: '/upload-chat',
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            async: true,
            timeout: 120000,
            success: function (data) {
                $("#spinner").hide();
                var messages = data.messages;
                var p_charts = data.pie;
                var l_charts = data.line
                for (i = 0; i < messages.length; i++) {
                    category = messages[i][0];
                    message = messages[i][1];
                    flash(message, category);
                }
                $('#messages').show();
                if (typeof p_charts !== 'undefined') {
                    for (i = 0; i < p_charts.length; i++) {
                        show_graphs(p_charts[i]);
                    }
                }
                if (typeof l_charts !== 'undefined') {
                    for (i = 0; i < l_charts.length; i++) {
                        show_line_graphs(l_charts[i]);
                    }
                    $('#upload-file').hide();
                    $('<button type="button" onclick="javascript:location.href = \'' + home + '\';"><i class="fa fa-refresh" aria-hidden="true"></i> analyze another</button>').appendTo("#messages");
                    $("#tip").hide();
                }
            },
            error: function (e) {
                $('#messages').show();
                $("#spinner").hide();
                flash("An error occurred analyzing data! The file may be too big.", "error");
            }
        });
    });
});

function show_graphs(graph) {
    var col = $('<div class="four columns"></div>');
    $('<embed type="image/svg+xml" src="' + graph + '" />').appendTo(col);
    col.appendTo("#pie");
}

function show_line_graphs(graph) {
    var col = $('<div class="12 columns"></div>');
    $('<embed type="image/svg+xml" src="' + graph + '" />').appendTo(col);
    col.appendTo("#line");
}

function flash(message, category) {
    if (category == 'error'){
        icon = 'fa-ban';
    }
    else if (category == 'warning'){
        icon = 'fa-warning';
    }
    else if (category == 'info'){
        icon = 'fa-info-circle';
    }
    else if (category == 'success'){
        icon = 'fa-check';
    }

    // Hook up x button dismissal
    var exit_button = $('<i class="fa fa-times exit-button"></i>').click(function () {
        var div = this.parentElement;
        div.style.opacity = "0";
        setTimeout(function () { div.style.display = "none"; }, 600);
    });

    var f_message = $('<div class="message-box message-box-' + category + '">' +
        '<i class="fa ' + icon + '"></i>' +
        '<span class="message-text"><strong>' + category + ': </strong>' + message + '</span></div>')

    exit_button.prependTo(f_message);
    f_message.prependTo('#messages');
    message_list.push(f_message);
}