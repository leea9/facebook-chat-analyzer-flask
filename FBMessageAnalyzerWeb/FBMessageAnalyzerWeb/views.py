﻿"""
Routes and views for the flask application.
"""

import os

from datetime import datetime
from flask import render_template, request, redirect, url_for, \
    flash, get_flashed_messages, jsonify
from FBMessageAnalyzerWeb import app
from werkzeug import secure_filename
from FBMessageAnalyzerWeb.modules.FBMessageCounter import FbMessageCounter

ALLOWED_EXTENSIONS = set(['txt', 'htm'])

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

@app.route('/')
@app.route('/home')
def home():
    """Renders the home page."""
    return render_template(
        'index.html',
        title='Home Page',
        year=datetime.now().year,
    )

@app.route("/upload-chat", methods=['POST'])
def upload():
    """ Handles the uploading and analysis of the chat file """
    if request.method == 'POST':
        result = {'parsed': None}
        if 'file' not in request.files:
            flash('No file was uploaded!', 'error')
        else:
            f = request.files['file']
            if f.filename == '':
                flash('No file was selected!', 'error')
            else:
                if f and allowed_file(f.filename):
                    counter = FbMessageCounter()
                    filename = secure_filename(f.filename)
                    convo = counter.parse_conversation(f.read())
                    flash('File parsed in {0:.3f}s!'.format(convo[1]), 'success')
                    p_charts = convo[0]['pie']
                    l_charts = convo[0]['line']
                    result['pie'] = p_charts
                    result['line'] = l_charts
                else:
                    flash('Not a supported filetype!', 'error')
        result['messages'] = get_flashed_messages(with_categories=True)
        return jsonify(result)


@app.route('/help')
def help():
    """Renders the help page."""
    return render_template(
        'help.html',
        title='Help',
        year=datetime.now().year
    )

@app.route('/about')
def about():
    """Renders the about page."""
    return render_template(
        'about.html',
        title='About',
        year=datetime.now().year
    )
