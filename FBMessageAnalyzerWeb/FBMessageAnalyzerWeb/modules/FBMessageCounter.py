﻿import random
import string
import pygal
import time
from pygal import style
from html.parser import HTMLParser
from datetime import datetime, timezone
from collections import OrderedDict
from bs4 import BeautifulSoup

class FbMessage:
    def __init__(self, ts, msg, st, em):
        # Timestamp is in UTC, convert to local time
        self.timestamp = ts
        self.messages = msg
        self.stickers = st
        self.emoticons = em

    def get_timestamp_datetime(self):
        return datetime.fromtimestamp(self.timestamp / 1000.0).replace(tzinfo=timezone.utc)

class FbParticipant:
    def __init__(self, name, message=None):
        self.name = name
        self.sticker_count = 0
        self.message_count = 0
        # Emoticons are those that show a graphical face in the messenger,
        #  not just ascii
        self.emoticon_count = 0
        self.message_list = []

        if message is not None:
            self.add_message(message)

    def add_message(self, fbmessage):
        if fbmessage.stickers is not None:
            self.sticker_count += len(fbmessage.stickers)
            self.message_count += 1
        else:
            if fbmessage.messages is not None:
                self.message_count += len(fbmessage.messages)
            if fbmessage.emoticons is not None:
                self.emoticon_count += len(fbmessage.emoticons)

        self.message_list.append(fbmessage)

class FbMessageCounter:
    def __init__(self):
        self.message_dict = {}
        self.color_array = [('#F3C178', '#00A878', '#D7FFDF', '#77B5A6', '#ACE6C0', '#9D4545'), 
                            ('#1C6E8C', '#274156', '#EAEFC4', '#9BDF46', '#25A55F', '#346473'), 
                            ('#3A7D44', '#254D32', '#00E0FF', '#74F9FF', '#A6FFF2', '#E8FFE8'),
                            ('#4F518C', '#2C2A4A', '#007AB5', '#005A85', '#004262', '#D8E6EC'),
                            ('#F1E9DB', '#5DB7DE', '#07588A', '#6AC1B8', '#BFE9DB', '#E1F6F4'),
                            ('#BC96E6', '#55286F', '#64868E', '#98B4A6', '#D1E4D1', '#F3FBF1'),
                            ('#55286F', '#BC96E6', '#414A50', '#85A6B1', '#8BD7D1', '#CAEDDE')]

    def create_pie_charts(self):
        if len(self.message_dict) == 0:
            return None
        # Make it so chats with more than 2 people
        # show the legend instead of printing labels
        elif len(self.message_dict) > 2:
            pie_kw = {
                'legend_at_bottom': True,
                'legend_at_bottom_columns': 2
                }
            tooltip_size = 30
            trans = False
        else:
            pie_kw = {
                'print_values': True,
                'print_labels': True,
                'show_legend': False
                }
            
            tooltip_size = 0
            trans = True

        types = ['message', 'sticker', 'emoticon']

        charts = []
        for c_type in types:
            random.seed(time.time())
            s_num = random.randint(0, len(self.color_array) - 1)
            style_kw = {
                "background": 'transparent',
                "title_font_size": 45,
                "value_font_size": 30,
                "value_label_font_size": 25,
                "tooltip_font_size": tooltip_size,
                "legend_font_size": 30,
                "value_colors": ('#19381F', '#FFF')
                }
            # Make the tooltip transparent if labels are shown
            if trans:
                style_kw["plot_background"] = 'transparent'
            g_style = style.LightGreenStyle(colors=self.color_array[s_num], **style_kw)
            pie_chart = pygal.Pie(style=g_style, **pie_kw)

            total = 0
            if c_type == 'message':
                for key, value in self.message_dict.items():
                    name = key.split(" ")[0]
                    total += value.message_count
                    data = [{"value": value.message_count, "label": name}]
                    pie_chart.add("{0} - {1}".format(name, value.message_count), data)
            elif c_type == 'sticker':
                for key, value in self.message_dict.items():
                    name = key.split(" ")[0]
                    total += value.sticker_count
                    data = [{"value": value.sticker_count, "label": name}]
                    pie_chart.add("{0} - {1}".format(name, value.sticker_count), data)
            elif c_type == 'emoticon':
                for key, value in self.message_dict.items():
                    name = key.split(" ")[0]
                    total += value.emoticon_count
                    data = [{"value": value.emoticon_count, "label": name}]
                    pie_chart.add("{0} - {1}".format(name, value.emoticon_count), data)
        
            pie_chart.title = "{0}s Sent (Total: {1})".format(c_type.capitalize(), total)
            charts.append(pie_chart.render_data_uri())

        return charts

    def message_over_time(self):
        style_kw = {
                "background": 'transparent',
                "title_font_size": 15,
                "tooltip_font_size": 10
                }
        g_style = style.LightGreenStyle(**style_kw)
        mot_chart = pygal.DateTimeLine(x_label_rotation=35, 
                                       x_value_formatter=lambda dt: dt.strftime('%Y-%m-%d'), 
                                       style=g_style,
                                       height=300,
                                       legend_at_bottom=True,
                                       show_y_guides=False,
                                       stroke_style={'width': 2},
                                       dots_size=1,
                                       legend_at_bottom_columns=4)
        mot_chart.title = "Messages By Day"

        for name, message_obj in self.message_dict.items():
            mot_data = OrderedDict()
            f_name = name.split(" ")[0]

            for m in message_obj.message_list:
                just_day = m.get_timestamp_datetime().replace(hour=0, minute=0, second=0, microsecond=0)
                if just_day in mot_data:
                    mot_data[just_day] += 1
                else:
                    mot_data[just_day] = 1

            mot_chart.add(f_name, list(mot_data.items()))
        return mot_chart.render_data_uri()

    def message_by_hour_of_day(self):
        random.seed(time.time())
        s_num = random.randint(0, len(self.color_array) - 1)
        style_kw = {
                "background": 'transparent',
                "title_font_size": 15,
                "tooltip_font_size": 10,
                "colors": self.color_array[s_num],
                }
        g_style = style.LightGreenStyle(**style_kw)
        moh_chart = pygal.Bar(legend_at_bottom=True,
                              show_y_guides=False,
                              style=g_style,
                              height=300,
                              legend_at_bottom_columns=4)
        moh_chart.title = "Messages By Hour Of The Day"
        moh_chart.x_labels = map(str, range(0, 24))
        
        for name, message_obj in self.message_dict.items():
            f_name = name.split(" ")[0]
            hour_dict = OrderedDict()
            for x in range(0, 24):
                hour_dict[x] = 0

            for m in message_obj.message_list:
                hr = m.get_timestamp_datetime().hour
                hour_dict[hr] += 1

            moh_chart.add("{}".format(f_name), [hour_dict[k] for k in hour_dict])
        return moh_chart.render_data_uri()

    def avg_message_dow(self):
        random.seed(time.time())
        s_num = random.randint(0, len(self.color_array) - 1)
        style_kw = {
                "background": 'transparent',
                "title_font_size": 15,
                "tooltip_font_size": 10,
                "colors": self.color_array[s_num],
                }
        g_style = style.LightGreenStyle(**style_kw)

        dow_chart = pygal.Bar(legend_at_bottom=True,
                              show_y_guides=False,
                              style=g_style,
                              height=300,
                              legend_at_bottom_columns=4)
        dow_chart.title = "Average Messages By Day of The Week"

        dow_chart.x_labels = ['Mon', 'Tues', 'Wed', 'Thurs', 'Fri', 'Sat', 'Sun']

        for name, message_obj in self.message_dict.items():
            f_name = name.split(" ")[0]
            cur_dow = 0
            num_dow = {}
            dow_dict = {}
            for m in message_obj.message_list:
                dow = m.get_timestamp_datetime().weekday()
                # Keep count of the amount of different day of the week for average count
                if dow != cur_dow:
                    if dow in num_dow:
                        num_dow[dow] += 1
                    else:
                        num_dow[dow] = 1
                    cur_dow = dow

                if dow in dow_dict:           
                    dow_dict[dow] += 1
                else:
                    dow_dict[dow] = 1

            dow_chart.add("{}".format(f_name), [int((dow_dict[k] / num_dow[k])) for k in sorted(dow_dict)])

        return dow_chart.render_data_uri()

    def parse_conversation(self, convo_file):
        try:
            start = time.time()
            convo = BeautifulSoup(convo_file, "lxml")
            end = time.time()
            html_time = end - start
            print("Time it took to parse html: {0:.3f}s".format(end - start))

            convos = convo.find_all('li', class_="webMessengerMessageGroup clearfix")
            start = time.time()
            for c in convos:
                timestamp = int(c.find("abbr", {"class": "_35 timestamp"})['data-utime'])
                name = c.find("strong", {"class": "_36"}).text.replace("\n", "")
                messages = [t.text.replace("\n", "") for t in c.find_all("p", class_="pClass")]
                emoticon = c.find_all("span", class_="emoticon")
                sticker = c.find("div", class_="_sq")
                message = FbMessage(timestamp, messages, sticker, emoticon)
                if name in self.message_dict:
                    self.message_dict[name].add_message(message)
                else:
                    self.message_dict[name] = FbParticipant(name, message)
            end = time.time()
            xml_time = end - start
            print("Time it took to parse messages: {0:.3f}s".format(end - start))
            total_time = html_time + xml_time

            charts = {}
            charts['pie'] = self.create_pie_charts()
            charts['line'] = [self.message_over_time(),
                              self.message_by_hour_of_day(),
                              self.avg_message_dow()]
            return (charts, total_time)
        except Exception as e:
            print("Exception occurred: {0}".format(e))