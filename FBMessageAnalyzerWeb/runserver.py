﻿"""
This script runs the FBMessageAnalyzerWeb application using a development server.
"""

from os import environ, urandom
import binascii
from FBMessageAnalyzerWeb import app

UPLOAD_FOLDER = 'uploads'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.secret_key = binascii.hexlify(urandom(24)).decode()

if __name__ == '__main__':
    HOST = environ.get('SERVER_HOST', 'localhost')
    try:
        PORT = int(environ.get('SERVER_PORT', '5555'))
    except ValueError:
        PORT = 5555
    # print("App Secret: {0}".format(app.secret_key))
    app.run(HOST, PORT, debug=False)
